/* NoDaveServer
 * Autor: Thomas Wiens, 2008
 * Adaptions: Tom Bruinink, 2009
 * 
 * Schnittstelle zur noDaveServer.dll
 * 
 * 
 * �nderungen
 * Tom Bruinink, 20090714
 * 
 * 1. lesen / schreiben auseinander gezogen um Lesbarkeit zu vergr��ern (weil die Funktionen sich ja erweitert haben, und die Lesbarkeit ins gedr�ngnis kam)
 * 2. IDisposable implementiert, um COM-object sicher zu zerst�ren. (ist in diesem fall nicht unbedingt notwendig. Es gilt aber der Regel:
 *    Wenn mann selber ein finalizer implementiert, dann auch IDisposable implementieren.
 * 3. PLC object mittels using-statement selber zerst�ren am ende des Threads, oder bei unhandled exception. Damit nicht mehr abh�ngig von GarbageCollector
 * 
 */
using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Forms;
using System.Diagnostics;
using System.IO;
using System.Runtime.InteropServices;
using System.Threading;

class noDaveServer : IDisposable
{
    bool disposed;
    
    [StructLayoutAttribute(LayoutKind.Sequential, CharSet = CharSet.Ansi)]
    public struct item_t
    {
        public byte area;
        public uint db;
        public byte transportSize;
        public uint bytepos;
        public byte bitpos;
        public uint length; // in Bytes
        public byte retCode;
        [MarshalAsAttribute(UnmanagedType.ByValArray, SizeConst = 1024)]
        public byte[] buf;
    }

    [DllImport("noDaveServer.dll")]
    private static extern int startNoDaveServer();

    [DllImport("noDaveServer.dll")]
    private static extern int cleanUpNoDaveServer();

    [DllImport("noDaveServer.dll")]
    private static extern int getItemCount();

    [DllImport("noDaveServer.dll")]
    private static extern int waitForRequests([In, Out] item_t[] item, ref int reqType);

    [DllImport("noDaveServer.dll")]
    private static extern void sendReadResponse([In, Out] item_t[] item );

    [DllImport("noDaveServer.dll")]
    private static extern void sendWriteResponse([In, Out] item_t[] item);

    [DllImport("noDaveServer.dll")]
    private static extern void printItems(item_t[] item);

    [DllImport("noDaveServer.dll")]
    private static extern void getClientAddress(StringBuilder strAddress); 

    public const Int16 daveFuncRead = 0x04;
    public const Int16 daveFuncWrite = 0x05;
    public enum TRANSPORT_SIZE : byte
    {
        // Typen mit 1 Byte L�nge
        BIT = 1, BYTE = 2, CHAR = 3,
        // Typen mit 2 Byte L�nge
        WORD = 4, INT = 5,
        // Typen mit 4 Byte L�nge
        DWORD = 6, DINT = 7, REAL = 8
    };

    /*
    Use these constants for parameter "area" in daveReadBytes and daveWriteBytes
    */
    public const int daveSysInfo = 0x3;	/* System info of 200 family */
    public const int daveSysFlags = 0x5;	/* System flags of 200 family */
    public const int daveAnaIn = 0x6;	/* analog inputs of 200 family */
    public const int daveAnaOut = 0x7;	/* analog outputs of 200 family */
    public const int daveP = 0x80;    	/* direct peripheral access */
    public const int daveInputs = 0x81;
    public const int daveOutputs = 0x82;
    public const int daveFlags = 0x83;
    public const int daveDB = 0x84;		/* data blocks */
    public const int daveDI = 0x85;	/* instance data blocks */
    public const int daveLocal = 0x86; 	/* not tested */
    public const int daveV = 0x87;	/* don't know what it is */
    public const int daveCounter = 28;	/* S7 counters */
    public const int daveTimer = 29;	/* S7 timers */
    public const int daveCounter200 = 30;	/* IEC counters (200 family) */
    public const int daveTimer200 = 31;	/* IEC timers (200 family) */

    public enum MSG_TYPE : int
    {
        MSG,    // Alles m�gliche
        ERROR,  // Fehler
        ITEM    // Itemnachrichten
    };
    private PLCSim PLC;
    private msgCallback msg;
    public bool Stop;
    public bool Running;
    public long ReadCounter;
    public long WriteCounter;
    item_t[] Item;
    //----------------------------------------------------------------------------------
    public noDaveServer(msgCallback mcb)
    {        
        Item = new item_t[80];
        msg = mcb;
        Running = false;
        ReadCounter = 0;
        WriteCounter = 0;
    }
    //----------------------------------------------------------------------------------
    ~noDaveServer()
    {
        Dispose(false);
    }
    //----------------------------------------------------------------------------------
    public delegate void msgCallback(int type, string log);
    //----------------------------------------------------------------------------------
    public void setPrintPLCSimAll(bool val)
    {
        if (PLC != null) PLC.printAllRead = val;
    }
    //----------------------------------------------------------------------------------
    public void setPrintItems(bool val)
    {
        if (PLC != null) PLC.printItems = val;
    }
    //----------------------------------------------------------------------------------
    public string getClientAddress()
    {
        StringBuilder strb = new StringBuilder(25);
        getClientAddress(strb);
        return strb.ToString();
    }
    //----------------------------------------------------------------------------------
    public string getPLCSimState()
    {
        if (PLC.isConnected() == true) return "Connected";
        return "Disconnected";
    }
    //----------------------------------------------------------------------------------
    public void run()
    {        
        int connected = 0;
        int reqType = 0;
        int res;
        Stop = false;

        try
        {
            PLC = new PLCSim(new PLCSim.msgCallback(msg));
        }
        catch (Exception ex)
        {
            MessageBox.Show("Error establishing connection to PLCSim!\r\n\r\n(Is PLCSim correctly installed?)\r\n\r\n" + ex, "Error");
            return;
        }

        Running = true;
        //@TBR 3
        using (PLC)
        {
            if (PLC.PLCSimConnect() == false)
            {
                msg((int)MSG_TYPE.ERROR, "Error! Connection to PLCSim could not be established!");
                return;
            }

            msg((int)MSG_TYPE.MSG, "Listening for clients...");          
            
            do
            {
                res = startNoDaveServer();
                if (res > 0)
                {
                    msg((int)MSG_TYPE.ERROR, ("Error! NoDaveServer could not be started! Return value: " + res));
                    switch (res)
                    {
                        case 1:
                            msg((int)MSG_TYPE.ERROR, (" Reason: Winsock could not be started"));
                            break;
                        case 2:
                            msg((int)MSG_TYPE.ERROR, (" Reason: Error creating socket"));
                            break;
                        case 3:
                            msg((int)MSG_TYPE.ERROR, (" Reason: Could not bind socket to port 102 (Port maybe in usage by another software?)"));
                            break;
                    }
                    cleanUpNoDaveServer();
                    Stop = true;
                }
                else if (res < 0)
                {
                    // -1 == timeout
                    cleanUpNoDaveServer();
                }
                else
                {
                    msg((int)MSG_TYPE.MSG, "Client (" + getClientAddress() + ") has connected... ");
                    do
                    {
                        connected = 1;
                        res = waitForRequests(Item, ref reqType);
                        if (res > 0)
                        {
                            Thread.CurrentThread.Priority = ThreadPriority.Normal;
                            if (reqType == daveFuncRead)
                            {
                                if (PLC.printItems == true) msg((int)MSG_TYPE.MSG, "*** Read-Request ***");
                                ReadCounter++;

                                PLC.PLCSimReadItems(Item, getItemCount());
                                sendReadResponse(Item);
                            }
                            else if (reqType == daveFuncWrite)
                            {
                                if (PLC.printItems == true) msg((int)MSG_TYPE.MSG, "*** Write-Request ***");
                                WriteCounter++;

                                PLC.PLCSimWriteItems(Item, getItemCount());
                                sendWriteResponse(Item);
                            }
                        }
                        else if (res == -1)
                        {
                            Thread.CurrentThread.Priority = ThreadPriority.Lowest;
                            msg((int)MSG_TYPE.MSG, "Server/client disconnected!");
                            connected = 0;
                        }
						else if (res == 0)
                        {
                            // Keine Anfrage gekommen -> erstmal schlafen legen
                            Thread.CurrentThread.Priority = ThreadPriority.Lowest;
                            Thread.Sleep(10);
                        }
                    } while ((connected == 1) && (Stop == false));

                    cleanUpNoDaveServer();
                }
            } while (Stop == false);

        }
        // PLC is now be disposed, whatever exceptions take place.
        // This will make sure that noDaveServer does not need to
        // clean up the PLC during its own finalization. 
        Running = false;
        PLC = null;
    }

    #region IDisposable Members
    /// <summary>
    /// Does the actual cleaning-up job
    /// </summary>
    /// <param name="disposing">true, if called by user, false if called by finalizer</param>
    private void Dispose(bool disposing)
    {
        //if not disposing, the references to other objects might already have been disposed of by GC.
        //In that case, is not allowed to refer to them here. 

        // if disposing, references to other objects are still valid
        if (disposing)//true if calling from user code, false if calling from finalizer
        {
            if (!disposed)//not called yet
            {
                try
                {
                    ;
                }
                catch (Exception ex)
                {
                    msg((int)MSG_TYPE.ERROR, ex.Message);
                }
            }
        }
        cleanUpNoDaveServer();//works on this object only, and may therefore be called here.
        this.disposed = true;
    }

    public void Dispose()
    {
        Dispose(true);
        GC.SuppressFinalize(this);
    }

    #endregion
}
