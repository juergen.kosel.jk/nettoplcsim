/* PLCSim
 * Autor: Thomas Wiens, 2008
 * Adaptions: Tom Bruinink, 2009
 * 
 * Stellt die Schnittstelle zu PLCSIM her.
 *
 * 
 * Erweiterungen:
 * Tom Bruinink, 20090714
 * 
 * - lesen / schreiben auseinander gezogen um Lesbarkeit zu vergr��ern (weil die Funktionen sich ja erweitert haben, und die Lesbarkeit ins gedr�ngnis kam)
 * - IDisposable implementiert, um COM-object sicher zu zerst�ren.
 * - gelesene / geschriebene Werte mit aufs GUI schicken (hat mir zumindest bei debugging sehr geholfen)
 * 
 * - Todo: untersuchen ob die jetzige Implemmentation vielleicht noch kompakter notiert werden kann.
 *         Sie ist wie die Engl�nder sagen: straightforward, und ggf nicht leistungsoptimal.
 */

using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Forms;
using S7PROSIMLib;

// Verweis auf Datei: "C:\SIEMENS\PLCSim\s7wsi\s7wspsmx.dll"
class PLCSim : IDisposable
{
    private bool disposed = false;
    private S7ProSim ps = new S7ProSim();
    private msgCallback msg;
    private bool psConnected;
    private bool itemError;
    public bool printAllRead;
    public bool printItems;
    private const byte DATA_OK = 0xff;
    private const byte DATA_ERROR = 0x0a;
    //----------------------------------------------------------------------------------
    public bool isConnected()
    {
        return psConnected;
    }
    //----------------------------------------------------------------------------------
    public PLCSim(msgCallback mcb)
    {
        psConnected = false;
        // Event-Handler registrieren
        // http://msdn.microsoft.com/de-de/library/341697aa(VS.80).aspx
        ps.ConnectionError += new IS7ProSimEvents_ConnectionErrorEventHandler(PLCSimConnectionError);
        msg = mcb;
    }
    //----------------------------------------------------------------------------------
    ~PLCSim()
    {
        //just call dispose, indicating that the call comes from the finalizer, instead of user code.
        Dispose(false);
    }
    //----------------------------------------------------------------------------------
    public delegate void msgCallback(int type, string log);
    //----------------------------------------------------------------------------------
    public bool PLCSimConnect()
    {
        psConnected = true;
        try
        {
            ps.Connect();
            ps.Continue();
            // Nach dem Connect steht PLCSIM im Singlestep-Modus, darum wieder zur�cksetzen
            ps.SetScanMode(ScanModeConstants.ContinuousScan);            
        }
        catch (Exception ex) 
        {
            psConnected = false;
            MessageBox.Show("Connection to PLCSim could not be established!\r\n" + ex, "Error");            
        };
        return psConnected;
    }
    //----------------------------------------------------------------------------------
    public void PLCSimDisconnect()
    {
        if (psConnected)
            ps.Disconnect();
        psConnected = false;
    }
    //----------------------------------------------------------------------------------

    /// <summary>
    /// Reads a set of requested items from PlcSim
    /// </summary>
    /// <param name="item">the array of items (requested from libnodave)</param>
    /// <param name="itemCount">the number of valid items in the array of items</param>
    /// <returns>allways 0</returns>
    public int PLCSimReadItems(noDaveServer.item_t[] item, int itemCount)
    {
        int db, bytepos, bitpos, area, transportSize;
        string itemMsg;
        object pData;
        if (psConnected == false)
            PLCSimConnect();

        #region loop through items
        for (int i = 0; i < itemCount; i++)
        {
            pData = null; // Initialize read memory to null for every item! Otherwise, we might return the previous item data!
            db = Convert.ToInt32(item[i].db);
            bytepos = Convert.ToInt32(item[i].bytepos);
            bitpos = Convert.ToInt32(item[i].bitpos);
            area = Convert.ToInt32(item[i].area);
            transportSize = Convert.ToByte(item[i].transportSize);

            itemMsg = ("Item[" + (i + 1) + "]  -  ");

            itemMsg += getItemRequestString(ref item[i]);
            if (printItems == true)
                msg((int)noDaveServer.MSG_TYPE.ITEM, itemMsg);

            if (checkIsAreaSupported(ref item[i]) == false) // check if area is supported
            {
                item[i].retCode = DATA_ERROR; //  0x0A = Item nicht verf�gbar
                item[i].length = 0;
                continue;  // to next item
            }

            itemError = false;

            #region Determine what to read
            uint nrDwords = 0;
            uint nrWords = 0;
            uint nrBytes = 0;
            
            nrDwords = item[i].length / 4;
            nrWords = (item[i].length % 4) / 2; 
            nrBytes = ((item[i].length % 4) % 2); //= nrWords % 2;
            #endregion

            //Read as many DWords as possible
            #region loop DWORDS
            for (int p = 0; p < nrDwords; p++)
            {
                if (ReadPLCSim(ref pData, area, db, bytepos, bitpos, (int)noDaveServer.TRANSPORT_SIZE.DWORD))
                {
                    try
                    {
                        // Convert the 4 bytes read from PlcSim
                        // Array reversion is necessary only since libNoDave expects Motorola on the other end of the connection
                        // Bestimmt unperformant, aber immerhin wird jetzt doch etwa die 4-fache geschwindigkeit erzielt
                        byte[] data = BitConverter.GetBytes((int)pData);
                        Array.Reverse(data);
                        Array.Copy(data, 0, item[i].buf, 4 * p, 4);
                    }
                    catch (Exception ex)
                    {
                        itemError = true;
                        msg((int)noDaveServer.MSG_TYPE.ERROR, itemMsg + " :DWORD[" + bytepos + "] - Error converting data read from PLCSim. Exception: " + ex.ToString());
                    }
                }
                else
                {
                    itemError = true;
                    msg((int)noDaveServer.MSG_TYPE.ERROR, itemMsg + " :DWORD[" + bytepos + "] - Error");
                }
                bytepos = bytepos + 4;
            }
            #endregion 

            #region process single WORD
            //nrofwords can only be 1 or 0
            if (nrWords == 1)
            {
                if (ReadPLCSim(ref pData, area, db, bytepos, bitpos, (int)noDaveServer.TRANSPORT_SIZE.WORD))
                {
                    try
                    {
                        // Convert the 4 bytes read from PlcSim
                        // Array reversion is necessary only since libNoDave expects Motorola on the other end of the connection
                        // Bestimmt unperformant, aber immerhin wird jetzt doch etwa die 2-fache geschwindigkeit erzielt
                        byte[] data = BitConverter.GetBytes((short)pData);
                        Array.Reverse(data);
                        Array.Copy(data, 0, item[i].buf, (4 * nrDwords), 2);
                    }
                    catch (Exception ex)
                    {
                        itemError = true;
                        msg((int)noDaveServer.MSG_TYPE.ERROR, itemMsg + " :WORD[" + bytepos + "] - Error converting data read from PLCSim. Exception: " + ex.ToString());
                    }
                }
                else
                {
                    itemError = true;
                    msg((int)noDaveServer.MSG_TYPE.ERROR, itemMsg + " :WORD[" + bytepos + "] - Error");
                }

                bytepos = bytepos + 2;
            }
            #endregion

            #region process single BYTE/BIT
            //nrBytes can only be 1 or 0
            if (nrBytes == 1)
            {
                bool res = false;
                //Special: if read bit is requested
                if (transportSize == (int)noDaveServer.TRANSPORT_SIZE.BIT)
                {
                    res = ReadPLCSim(ref pData, area, db, bytepos, bitpos, (int)noDaveServer.TRANSPORT_SIZE.BIT);
                }
                else
                {
                    res = ReadPLCSim(ref pData, area, db, bytepos, bitpos, (int)noDaveServer.TRANSPORT_SIZE.BYTE);
                }
                if (res)
                {
                    try
                    {
                        item[i].buf[(4 * nrDwords + 2 * nrWords)] = Convert.ToByte(pData);
                    }
                    catch (Exception ex)
                    {
                        itemError = true;
                        msg((int)noDaveServer.MSG_TYPE.ERROR, itemMsg + " :BYTE[" + bytepos + "] - Error converting data read from PLCSim. Exception: " + ex.ToString());
                    }
                }
                else
                {
                    itemError = true;
                    msg((int)noDaveServer.MSG_TYPE.ERROR, itemMsg + " :BYTE[" + bytepos + "] - Error");
                }

                bytepos = bytepos + 1;
            }
            #endregion

            #region process item result
            if (itemError == false) // Wenn bis hierhin alles OK, dann auch das Item OK, evtl. kann bei erstem Fehler gleich das weitere Lesen aufgegeben werden.
                item[i].retCode = DATA_OK; // 0xff = OK

            else
            {
                item[i].retCode = DATA_ERROR; //  0x0A = Item nicht verf�gbar
                item[i].length = 0;
            }
            #endregion
        }
        #endregion
        return 0;
    }

    /// <summary>
    /// Writes a set of requested items to PlcSim
    /// </summary>
    /// <param name="item">the array of items (requested from libnodave)</param>
    /// <param name="itemCount">the number of valid items in the array of items</param>
    /// <returns>allways 0</returns>
    public int PLCSimWriteItems(noDaveServer.item_t[] item, int itemCount)
    {
        int db, bytepos, bitpos, area, transportSize;
        string itemMsg;
        object pData;
        if (psConnected == false)
            PLCSimConnect();

        #region loop through items
        for (int i = 0; i < itemCount; i++)
        {
            pData = null; // Initialize write memory to null for every item. Otherwise, we might send the previous item data!
            db = Convert.ToInt32(item[i].db);
            bytepos = Convert.ToInt32(item[i].bytepos);
            bitpos = Convert.ToInt32(item[i].bitpos);
            area = Convert.ToInt32(item[i].area);
            transportSize = Convert.ToByte(item[i].transportSize);

            itemMsg = ("Item[" + (i + 1) + "]  -  ");

            itemMsg += getItemRequestString(ref item[i]);
            if (printItems == true)
                msg((int)noDaveServer.MSG_TYPE.ITEM, itemMsg);

            if (checkIsAreaSupported(ref item[i]) == false) // check if area is supported
            {
                item[i].retCode = DATA_ERROR; //  0x0A = Item nicht verf�gbar
                item[i].length = 0;
                continue;  // to next item
            }

            itemError = false;

            #region Determine what to read
            uint nrDwords = 0;
            uint nrWords = 0;
            uint nrBytes = 0;

            nrDwords = item[i].length / 4;
            nrWords = (item[i].length % 4) / 2;
            nrBytes = ((item[i].length % 4) % 2);

            //nrBytes = item[i].length;
            #endregion

            #region loop DWORDS
            // Array reversion is necessary since libNoDave expects Motorola on the other end of the connection
            for (int p = 0; p < nrDwords; p = p + 1)
            {
                // Catch the 4 bytes to write from the item buffer
                // Bestimmt unperformant, aber immerhin wird jetzt doch etwa die 4-fache geschwindigkeit erzielt
                byte[] dataToWrite = new byte[4];
                Array.Copy(item[i].buf, 4 * p, dataToWrite, 0, 4);
                Array.Reverse(dataToWrite);
                
                pData = dataToWrite;

                if (!WritePLCSim(ref pData, area, db, bytepos, bitpos, (int)noDaveServer.TRANSPORT_SIZE.DWORD))
                {
                    itemError = true;
                    msg((int)noDaveServer.MSG_TYPE.ERROR, itemMsg + " :BYTE[" + bytepos + "]  - Error");
                }
                bytepos = bytepos + 4;
            }
            #endregion

            #region process single WORD
            // Array reversion is necessary since libNoDave expects Motorola on the other end of the connection
            if (nrWords != 0) //will never be > 1
            //for (int p = 0; p < nrWords; p = p + 1)
            {
                // Bestimmt unperformant, aber immerhin wird jetzt doch etwa die 2-fache geschwindigkeit erzielt
                byte[] dataToWrite = new byte[2];
                Array.Copy(item[i].buf, (4 * nrDwords), dataToWrite, 0, 2);
                Array.Reverse(dataToWrite);

                pData = dataToWrite;

                if (!WritePLCSim(ref pData, area, db, bytepos, bitpos, (int)noDaveServer.TRANSPORT_SIZE.WORD))
                {
                    itemError = true;
                    msg((int)noDaveServer.MSG_TYPE.ERROR, itemMsg + " :BYTE[" + bytepos + "]  - Error");
                }

                bytepos = bytepos + 2;
            }
            #endregion

            #region process single BYTE/BIT
            if (nrBytes != 0) //will never be > 1
            //for (int p = 0; p < nrBytes; p++)
            {
                //Special: if write bit is requested
                if (transportSize == 1)
                {   // Bei Bittypen kann nur ein Bit in einem Item liegen
                    pData = Convert.ToBoolean(item[i].buf[4 * nrDwords + 2 * nrWords]);
                }
                else
                {
                    pData = item[i].buf[4 * nrDwords + 2 * nrWords];
                }
                if (!WritePLCSim(ref pData, area, db, bytepos, bitpos, transportSize))
                {
                    itemError = true;
                    msg((int)noDaveServer.MSG_TYPE.ERROR, itemMsg + " :BYTE[" + bytepos + "]  - Error");
                }
                bytepos++;
            }
            #endregion

            #region process item result
            if (itemError == false) // Wenn bis hierhin alles OK, dann auch das Item OK, evtl. kann bei erstem Fehler gleich das weitere Lesen aufgegeben werden.
                item[i].retCode = DATA_OK; // 0xff = OK

            else
            {
                item[i].retCode = DATA_ERROR; //  0x0A = Item nicht verf�gbar
                item[i].length = 0;
            }
            #endregion
        } // for i
        #endregion

        return 0;
    }

    /// <summary>
    /// Reads part of item from PlcSim. Parts defined by PlcSim API
    /// </summary>
    /// <param name="pData"></param>
    /// <param name="area"></param>
    /// <param name="db"></param>
    /// <param name="bytepos"></param>
    /// <param name="bitpos"></param>
    /// <param name="transportSize"></param>
    /// <returns></returns>
    bool ReadPLCSim(ref System.Object pData, int area, int db, int bytepos, int bitpos, int transportSize)
    {
        PointDataTypeConstants pdtConst;

        string strRW = "  Read";
 
        string sizestring;

        switch (transportSize)
        {
            case (int)noDaveServer.TRANSPORT_SIZE.BIT: pdtConst = PointDataTypeConstants.S7_Bit; sizestring = "X"; break;
            case (int)noDaveServer.TRANSPORT_SIZE.BYTE: pdtConst = PointDataTypeConstants.S7_Byte; sizestring = "B"; break;
            case (int)noDaveServer.TRANSPORT_SIZE.WORD: pdtConst = PointDataTypeConstants.S7_Word; sizestring = "W"; break;
            case (int)noDaveServer.TRANSPORT_SIZE.DWORD: pdtConst = PointDataTypeConstants.S7_DoubleWord; sizestring = "D"; break;
            default: pdtConst = PointDataTypeConstants.S7_Byte; sizestring = "B"; break;
        }
        string itemSubstring = getItemSubString(area, db, bytepos, bitpos, sizestring);

        // read the data
        bool subitemError = false;
        switch (area)
        {
            case (byte)noDaveServer.daveDB: ps.ReadDataBlockValue(db, bytepos, bitpos, pdtConst, ref pData); if (pData == null) { subitemError = true; } break;
            case (byte)noDaveServer.daveFlags: ps.ReadFlagValue(bytepos, bitpos, pdtConst, ref pData); if (pData == null) { subitemError = true; } break;
            case (byte)noDaveServer.daveOutputs: ps.ReadOutputPoint(bytepos, bitpos, pdtConst, ref pData); if (pData == null) { subitemError = true; } break;
            case (byte)noDaveServer.daveInputs: subitemError = true; break;
        }
        // return false if no data received
        if (subitemError) 
        {
            if (printAllRead) msg((int)noDaveServer.MSG_TYPE.ITEM, strRW + itemSubstring + " returned no data"); 
            return !(subitemError); 
        }

        // compose string to return
        string valuestring;
        if (printAllRead)
        {
            switch (transportSize)
            {
                case (int)noDaveServer.TRANSPORT_SIZE.BIT: valuestring = string.Format("  : {0:X1}", pData); break;
                case (int)noDaveServer.TRANSPORT_SIZE.BYTE: valuestring = string.Format("  : 0x{0:X2}", pData); break;
                case (int)noDaveServer.TRANSPORT_SIZE.WORD: valuestring = string.Format("  : 0x{0:X4}", pData); break;
                case (int)noDaveServer.TRANSPORT_SIZE.DWORD: valuestring = string.Format("  : 0x{0:X8}", pData); break;
                default: sizestring = "B"; valuestring = string.Format("  : 0x{0:X8}", (byte)pData); break;
            }
            msg((int)noDaveServer.MSG_TYPE.ITEM, strRW + itemSubstring + valuestring);
        }

        //return true
        return !(subitemError);
    }

    /// <summary>
    /// Writes part of item to PlcSim. Parts defined by PlcSim API
    /// </summary>
    /// <param name="pData"></param>
    /// <param name="area"></param>
    /// <param name="db"></param>
    /// <param name="bytepos"></param>
    /// <param name="bitpos"></param>
    /// <param name="transportSize"></param>
    /// <returns>true if succesful</returns>
    bool WritePLCSim(ref System.Object pData, int area, int db, int bytepos, int bitpos, int transportSize)
    {
        string strRW = "  Write";

        string sizestring;
        string valuestring;
        object dataToWrite;
        switch (transportSize)
        {
            // TBR: After some tests it appears that: 
            // S7ProSim accepts only bit, byte, short and int (not byte[], ushort, uint, or other types with same size). Therefore, cast to those types
            case (int)noDaveServer.TRANSPORT_SIZE.BIT: dataToWrite = (bool)pData; break;
            case (int)noDaveServer.TRANSPORT_SIZE.BYTE: dataToWrite = (byte)pData; break;
            case (int)noDaveServer.TRANSPORT_SIZE.WORD: dataToWrite = BitConverter.ToInt16((byte[])pData, 0); break;
            case (int)noDaveServer.TRANSPORT_SIZE.DWORD: dataToWrite = BitConverter.ToInt32((byte[])pData, 0); break;
            default: dataToWrite = (byte)pData; break;
        }

        if (printAllRead)
        {
            switch (transportSize)
            {
                case (int)noDaveServer.TRANSPORT_SIZE.BIT: sizestring = "X"; valuestring = string.Format("  : {0:X1}", pData, 0); break;
                case (int)noDaveServer.TRANSPORT_SIZE.BYTE: sizestring = "B"; valuestring = string.Format("  : 0x{0:X2}", (byte)pData, 0); break;
                case (int)noDaveServer.TRANSPORT_SIZE.WORD: sizestring = "W"; valuestring = string.Format("  : 0x{0:X4}", dataToWrite, 0); break;
                case (int)noDaveServer.TRANSPORT_SIZE.DWORD: sizestring = "D"; valuestring = string.Format("  : 0x{0:X8}", dataToWrite, 0); break;
                default: sizestring = "B"; valuestring = string.Format("  : 0x{0:X8}", (byte)pData, 0); break;
            }
            string itemSubstring = getItemSubString(area, db, bytepos, bitpos, sizestring);
            msg((int)noDaveServer.MSG_TYPE.ITEM, strRW + itemSubstring + valuestring);
        }

        bool subitemError = false;
        switch (area)
        {
            case (byte)noDaveServer.daveDB: ps.WriteDataBlockValue(db, bytepos, bitpos, ref dataToWrite); break;
            case (byte)noDaveServer.daveFlags: ps.WriteFlagValue(bytepos, bitpos, ref dataToWrite); break;
            case (byte)noDaveServer.daveOutputs: subitemError = true; break;
            case (byte)noDaveServer.daveInputs: ps.WriteInputPoint(bytepos, bitpos, ref dataToWrite); break; 
        }
        return !(subitemError);
    }

    /// <summary>
    /// Provides a string describing the single PlcSim read/write request address
    /// </summary>
    /// <param name="area"></param>
    /// <param name="db"></param>
    /// <param name="bytepos"></param>
    /// <param name="bitpos"></param>
    /// <param name="sizestring"></param>
    /// <returns></returns>
    private string getItemSubString(int area, int db, int bytepos, int bitpos, string sizestring)
    {
        switch (area)
        {
            case (byte)noDaveServer.daveDB: return String.Format(" PLCSim: DB{0}.DB{1}{2}.{3}", db, sizestring, bytepos,bitpos);
            case (byte)noDaveServer.daveFlags: return String.Format(" PLCSim: M{0}{1}.{2}", sizestring, bytepos,bitpos);
            case (byte)noDaveServer.daveOutputs: return String.Format(" PLCSim: Q{0}{1}.{2}", sizestring, bytepos,bitpos);
            case (byte)noDaveServer.daveInputs: return String.Format(" PLCSim: I{0}{1}.{2}",sizestring, bytepos,bitpos);
            default: return "no valid area";
        }
    }

    #region IDisposable Members

    private void Dispose(bool disposing)
    {
        //if not disposing, the references to other objects might already have been disposed of by GC.
        //In that case, is not allowed to refer to them here. (For this reason, it is wise to destruct this object 
        //from user code, instead of relying on GC, since we handle non-managed resources here....)

        // if disposing, references to other objects are still valid
        if (disposing)//if calling from user code, (not finalizer)
        {
            if (!disposed)//not called yet
            {
                try
                {
                    ps.Disconnect();
                }
                catch (Exception ex)
                {
                    if (msg != null) msg((int)noDaveServer.MSG_TYPE.ERROR, ex.Message); 
                }
            }
        }
        this.disposed = true;
    }

    public void Dispose()
    {
         Dispose(true);
         GC.SuppressFinalize(this);
    }

    #endregion



    //----------------------------------------------------------------------------------
    // Die L�nge im item ist immer als Anzahl der Bytes
    // String z.B. "DB5.DBX0.0 BYTE 10" oder "M10.0 BYTE 10"
    string getItemRequestString(ref noDaveServer.item_t item)
    {
        string strAddr = "";
        switch (item.area)
        {
            case (byte)noDaveServer.daveDB:
                strAddr = "DB" + item.db + ".DBX" + item.bytepos + "." + item.bitpos;
                break;
            case (byte)noDaveServer.daveFlags:
                strAddr = "M" + item.bytepos + "." + item.bitpos;
                break;
            case (byte)noDaveServer.daveOutputs:
                strAddr = "Q" + item.bytepos + "." + item.bitpos;
                break;
            case (byte)noDaveServer.daveInputs:
                strAddr = "I" + item.bytepos + "." + item.bitpos;
                break;
            case (byte)noDaveServer.daveTimer:
                strAddr = "T" + ((item.bytepos * 8) + item.bitpos);
                break;
            case (byte)noDaveServer.daveCounter:
                strAddr = "C" + ((item.bytepos * 8) + item.bitpos);
                break;
        }
        if (item.transportSize != (byte)noDaveServer.TRANSPORT_SIZE.BIT)
        {
            strAddr += " BYTE " + item.length;
        }
        return strAddr;
    }
    //----------------------------------------------------------------------------------
    // �berpr�fen ob dieser Bereich �berhaupt unterst�tzt wird
    bool checkIsAreaSupported(ref noDaveServer.item_t item)
    {
        switch (item.area)
        {
            case (byte)noDaveServer.daveDB:
            case (byte)noDaveServer.daveFlags:
            case (byte)noDaveServer.daveOutputs:
            case (byte)noDaveServer.daveInputs:
                return true;
            case (byte)noDaveServer.daveTimer:
                msg((int)noDaveServer.MSG_TYPE.ERROR, "Timers are not supported!");
                return false;
            case (byte)noDaveServer.daveCounter:
                msg((int)noDaveServer.MSG_TYPE.ERROR, "Counters are not supported!");
                return false;
            default:
                msg((int)noDaveServer.MSG_TYPE.ERROR, "Area: " + item.area + " is not supported!");
                return false;
        }
    }
    //----------------------------------------------------------------------------------
    void PLCSimConnectionError(string ControlEngine, int Error)
    {
        msg((int)noDaveServer.MSG_TYPE.ERROR, "PLCSimConnectionError(): ");
        switch ((uint)Error)
        {
            case (uint)0x00000000L:
                msg((int)noDaveServer.MSG_TYPE.MSG, "S_OK - Success code"); //Erfolgscode");
                break;
            case (uint)0x80004005L:
                msg((int)noDaveServer.MSG_TYPE.ERROR, "E_FAIL - Unspecified error"); // Nicht angegebener Fehler");
                itemError = true;
                break;
            case (uint)0x80040211L:
                msg((int)noDaveServer.MSG_TYPE.ERROR, "PS_E_NOTCONNECTED - S7ProSim is not connected to S7-PLCSIM");//S7ProSim ist nicht mit S7-PLCSIM verbunden");
                itemError = true;
                psConnected = false;
                PLCSimDisconnect();
                break;
            case (uint)0x80040212L:
                msg((int)noDaveServer.MSG_TYPE.ERROR, "PS_E_POWEROFF - S7-PLCSIM is powered off");//S7-PLCSIM ist ausgeschaltet");
                itemError = true;
                psConnected = false;
                PLCSimDisconnect();
                break;
            case (uint)0x80040206L:
                msg((int)noDaveServer.MSG_TYPE.ERROR, "PS_E_BADTYPE - Invalid data type");//Ung�ltiger Datentyp");
                itemError = true;
                break;
            case (uint)0x80040201L:
                msg((int)noDaveServer.MSG_TYPE.ERROR, "PS_E_BADBYTENDX - Byte index is invalid");//Byteindex ist ung�ltig");
                itemError = true;
                break;
            case (uint)0x80040202L:
                msg((int)noDaveServer.MSG_TYPE.ERROR, "PS_E_BADBYTECOUNT - Size of data array is invalid for given starting byte index");//Gr��e des Daten-Array ist f�r den angegebenen Anfangs-Byteindex ung�ltig");
                itemError = true;
                break;
            case (uint)0x80040203L:
                msg((int)noDaveServer.MSG_TYPE.ERROR, "PS_E_READFAILED - Read operation failed");//Leseoperation fehlgeschlagen");
                itemError = true;
                break;
            default:
                msg((int)noDaveServer.MSG_TYPE.ERROR, "Unknown errorcode: " + Error);
                itemError = true;
                break;
        }
    }
}