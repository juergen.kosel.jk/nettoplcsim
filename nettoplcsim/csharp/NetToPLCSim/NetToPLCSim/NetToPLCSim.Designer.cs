﻿namespace NetToPLCSim
{
    partial class frmNetToPLCSim
    {
        /// <summary>
        /// Erforderliche Designervariable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Verwendete Ressourcen bereinigen.
        /// </summary>
        /// <param name="disposing">True, wenn verwaltete Ressourcen gelöscht werden sollen; andernfalls False.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Vom Windows Form-Designer generierter Code

        /// <summary>
        /// Erforderliche Methode für die Designerunterstützung.
        /// Der Inhalt der Methode darf nicht mit dem Code-Editor geändert werden.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmNetToPLCSim));
            this.btnStart = new System.Windows.Forms.Button();
            this.btnStop = new System.Windows.Forms.Button();
            this.tbItemMsg = new System.Windows.Forms.TextBox();
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.cbPrintAll = new System.Windows.Forms.CheckBox();
            this.statusStrip1 = new System.Windows.Forms.StatusStrip();
            this.toolStripLabelPLCSim = new System.Windows.Forms.ToolStripStatusLabel();
            this.toolStripLabelClient = new System.Windows.Forms.ToolStripStatusLabel();
            this.cbPrintItems = new System.Windows.Forms.CheckBox();
            this.tbReadCounter = new System.Windows.Forms.TextBox();
            this.tbWriteCounter = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.toolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.statusStrip1.SuspendLayout();
            this.menuStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // btnStart
            // 
            this.btnStart.Location = new System.Drawing.Point(12, 33);
            this.btnStart.Name = "btnStart";
            this.btnStart.Size = new System.Drawing.Size(143, 30);
            this.btnStart.TabIndex = 0;
            this.btnStart.Text = "Start";
            this.btnStart.UseVisualStyleBackColor = true;
            this.btnStart.Click += new System.EventHandler(this.btnStart_Click);
            // 
            // btnStop
            // 
            this.btnStop.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnStop.Location = new System.Drawing.Point(262, 33);
            this.btnStop.Name = "btnStop";
            this.btnStop.Size = new System.Drawing.Size(143, 30);
            this.btnStop.TabIndex = 1;
            this.btnStop.Text = "Stop";
            this.btnStop.UseVisualStyleBackColor = true;
            this.btnStop.Click += new System.EventHandler(this.btnStop_Click);
            // 
            // tbItemMsg
            // 
            this.tbItemMsg.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.tbItemMsg.Location = new System.Drawing.Point(12, 115);
            this.tbItemMsg.Multiline = true;
            this.tbItemMsg.Name = "tbItemMsg";
            this.tbItemMsg.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.tbItemMsg.Size = new System.Drawing.Size(390, 170);
            this.tbItemMsg.TabIndex = 3;
            // 
            // timer1
            // 
            this.timer1.Enabled = true;
            this.timer1.Interval = 1000;
            this.timer1.Tick += new System.EventHandler(this.timer1_Tick);
            // 
            // cbPrintAll
            // 
            this.cbPrintAll.AutoSize = true;
            this.cbPrintAll.Location = new System.Drawing.Point(12, 92);
            this.cbPrintAll.Name = "cbPrintAll";
            this.cbPrintAll.Size = new System.Drawing.Size(174, 17);
            this.cbPrintAll.TabIndex = 6;
            this.cbPrintAll.Text = "Display requested PLCSim data";
            this.cbPrintAll.UseVisualStyleBackColor = true;
            // 
            // statusStrip1
            // 
            this.statusStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripLabelPLCSim,
            this.toolStripLabelClient});
            this.statusStrip1.Location = new System.Drawing.Point(0, 322);
            this.statusStrip1.Name = "statusStrip1";
            this.statusStrip1.ShowItemToolTips = true;
            this.statusStrip1.Size = new System.Drawing.Size(414, 22);
            this.statusStrip1.TabIndex = 7;
            this.statusStrip1.Text = "statusStrip1";
            // 
            // toolStripLabelPLCSim
            // 
            this.toolStripLabelPLCSim.BackColor = System.Drawing.SystemColors.Control;
            this.toolStripLabelPLCSim.BorderSides = ((System.Windows.Forms.ToolStripStatusLabelBorderSides)((((System.Windows.Forms.ToolStripStatusLabelBorderSides.Left | System.Windows.Forms.ToolStripStatusLabelBorderSides.Top)
                        | System.Windows.Forms.ToolStripStatusLabelBorderSides.Right)
                        | System.Windows.Forms.ToolStripStatusLabelBorderSides.Bottom)));
            this.toolStripLabelPLCSim.BorderStyle = System.Windows.Forms.Border3DStyle.SunkenOuter;
            this.toolStripLabelPLCSim.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.toolStripLabelPLCSim.Name = "toolStripLabelPLCSim";
            this.toolStripLabelPLCSim.Size = new System.Drawing.Size(199, 17);
            this.toolStripLabelPLCSim.Spring = true;
            this.toolStripLabelPLCSim.Text = "PLCSim:";
            this.toolStripLabelPLCSim.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // toolStripLabelClient
            // 
            this.toolStripLabelClient.BorderSides = ((System.Windows.Forms.ToolStripStatusLabelBorderSides)((((System.Windows.Forms.ToolStripStatusLabelBorderSides.Left | System.Windows.Forms.ToolStripStatusLabelBorderSides.Top)
                        | System.Windows.Forms.ToolStripStatusLabelBorderSides.Right)
                        | System.Windows.Forms.ToolStripStatusLabelBorderSides.Bottom)));
            this.toolStripLabelClient.BorderStyle = System.Windows.Forms.Border3DStyle.SunkenOuter;
            this.toolStripLabelClient.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.toolStripLabelClient.Name = "toolStripLabelClient";
            this.toolStripLabelClient.Size = new System.Drawing.Size(199, 17);
            this.toolStripLabelClient.Spring = true;
            this.toolStripLabelClient.Text = "Client:";
            this.toolStripLabelClient.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // cbPrintItems
            // 
            this.cbPrintItems.AutoSize = true;
            this.cbPrintItems.Location = new System.Drawing.Point(12, 69);
            this.cbPrintItems.Name = "cbPrintItems";
            this.cbPrintItems.Size = new System.Drawing.Size(191, 17);
            this.cbPrintItems.TabIndex = 8;
            this.cbPrintItems.Text = "Display requested data area (items)";
            this.cbPrintItems.UseVisualStyleBackColor = true;
            // 
            // tbReadCounter
            // 
            this.tbReadCounter.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.tbReadCounter.Location = new System.Drawing.Point(99, 291);
            this.tbReadCounter.Name = "tbReadCounter";
            this.tbReadCounter.ReadOnly = true;
            this.tbReadCounter.Size = new System.Drawing.Size(83, 20);
            this.tbReadCounter.TabIndex = 9;
            this.tbReadCounter.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // tbWriteCounter
            // 
            this.tbWriteCounter.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.tbWriteCounter.Location = new System.Drawing.Point(319, 291);
            this.tbWriteCounter.Name = "tbWriteCounter";
            this.tbWriteCounter.ReadOnly = true;
            this.tbWriteCounter.Size = new System.Drawing.Size(83, 20);
            this.tbWriteCounter.TabIndex = 10;
            this.tbWriteCounter.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label1
            // 
            this.label1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(9, 294);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(84, 13);
            this.label1.TabIndex = 11;
            this.label1.Text = "Read-Requests:";
            // 
            // label2
            // 
            this.label2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(232, 294);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(83, 13);
            this.label2.TabIndex = 12;
            this.label2.Text = "Write-Requests:";
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripMenuItem1});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(414, 24);
            this.menuStrip1.TabIndex = 13;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // toolStripMenuItem1
            // 
            this.toolStripMenuItem1.Name = "toolStripMenuItem1";
            this.toolStripMenuItem1.Size = new System.Drawing.Size(39, 20);
            this.toolStripMenuItem1.Text = "Info";
            this.toolStripMenuItem1.Click += new System.EventHandler(this.toolStripMenuItem1_Click);
            // 
            // frmNetToPLCSim
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(414, 344);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.tbWriteCounter);
            this.Controls.Add(this.tbReadCounter);
            this.Controls.Add(this.cbPrintItems);
            this.Controls.Add(this.statusStrip1);
            this.Controls.Add(this.menuStrip1);
            this.Controls.Add(this.cbPrintAll);
            this.Controls.Add(this.tbItemMsg);
            this.Controls.Add(this.btnStop);
            this.Controls.Add(this.btnStart);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "frmNetToPLCSim";
            this.Text = "NetToPLCSim";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.frmNetToPLCSim_FormClosing);
            this.statusStrip1.ResumeLayout(false);
            this.statusStrip1.PerformLayout();
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnStart;
        private System.Windows.Forms.Button btnStop;
        private System.Windows.Forms.TextBox tbItemMsg;
        private System.Windows.Forms.Timer timer1;
        private System.Windows.Forms.CheckBox cbPrintAll;
        private System.Windows.Forms.StatusStrip statusStrip1;
        private System.Windows.Forms.ToolStripStatusLabel toolStripLabelPLCSim;
        private System.Windows.Forms.ToolStripStatusLabel toolStripLabelClient;
        private System.Windows.Forms.CheckBox cbPrintItems;
        private System.Windows.Forms.TextBox tbReadCounter;
        private System.Windows.Forms.TextBox tbWriteCounter;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem1;
    }
}

