/* NetToPLCSim
 * Netzwerkanbindung f�r PLCSIM
 * Copyright (C) 2008 Thomas Wiens, th.wiens@gmx.de
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 * 
 */
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Threading;

namespace NetToPLCSim
{
    public partial class frmNetToPLCSim : Form
    {
        noDaveServer server;
        Thread serverThread;
        int MsgType;
        string Msg;
        //----------------------------------------------------------------------------------
        public frmNetToPLCSim()
        {
            InitializeComponent();
        }
        //----------------------------------------------------------------------------------
        private void frmNetToPLCSim_FormClosing(object sender, FormClosingEventArgs e)
        {
            stopThread();
        }
        //----------------------------------------------------------------------------------
        private void btnStart_Click(object sender, EventArgs e)
        {
            startServer();
        }
        //----------------------------------------------------------------------------------
        private void btnStop_Click(object sender, EventArgs e)
        {
            stopThread();
        }
        //----------------------------------------------------------------------------------
        public void LogParse(int type, string log)
        {
            if (server.Stop == true) return; // Deadlock bei joinen des Threads verhindern
            Msg = log;
            MsgType = type;
            if (tbItemMsg.InvokeRequired)
                tbItemMsg.Invoke(new MethodInvoker(Log));
            else Log();
        }
        //----------------------------------------------------------------------------------
        private void Log()
        {
            tbItemMsg.AppendText((Msg + "\r\n"));
        }
        //----------------------------------------------------------------------------------
        private void startServer()
        {
            if (serverThread != null)
            {
                if (serverThread.ThreadState == ThreadState.Stopped)
                {
                    server.Stop = false;
                    serverThread = new Thread(server.run);
                    serverThread.Priority = ThreadPriority.Lowest;
                    serverThread.Start();
                }
            }
            if (server == null)
            {
                server = new noDaveServer(new noDaveServer.msgCallback(LogParse));
                serverThread = new Thread(server.run);
                serverThread.Start();
            }
        }
        //----------------------------------------------------------------------------------
        private void stopThread()
        {
            if (server != null)
            {
                if (serverThread.ThreadState == ThreadState.Running || serverThread.ThreadState == ThreadState.WaitSleepJoin)
                {
                    tbItemMsg.AppendText("Stopping server thread...");
                    server.Stop = true;
                    tbItemMsg.AppendText("waiting...\r\n");
                    serverThread.Join();
                    tbItemMsg.AppendText("Stopped.\r\n");
                }
            }
        }
        //----------------------------------------------------------------------------------
        private void timer1_Tick(object sender, EventArgs e)
        {
            string client = "Client: ";
            string plcsim = "PLCSim: ";
            if (server != null)
            {
                if (server.Running == true)
                {
                    plcsim += server.getPLCSimState();
                    client += server.getClientAddress();
                }
                else
                {
                    plcsim += "Disconnected";
                    client += "Disconnected";
                }

                server.setPrintPLCSimAll(cbPrintAll.Checked);
                server.setPrintItems(cbPrintItems.Checked);
                tbReadCounter.Text = server.ReadCounter.ToString();
                tbWriteCounter.Text = server.WriteCounter.ToString();
            }
            else
            {
                client += "Disconnected";
                plcsim += "Disconnected";
            }
            toolStripLabelPLCSim.Text = plcsim;
            toolStripLabelClient.Text = client;
        }

        private void toolStripMenuItem1_Click(object sender, EventArgs e)
        {
            frmInfo frmInfo = new frmInfo();
            frmInfo.ShowDialog();
        }
        //----------------------------------------------------------------------------------
    }
}