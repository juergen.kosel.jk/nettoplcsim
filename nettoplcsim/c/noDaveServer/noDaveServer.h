/* noDaveServer.h
 *
 * Autor: Thomas Wiens, November 2008
 *
 */
#ifndef NODAVESERVER_HEADER_INCLUDED
#define NODAVESERVER_HEADER_INCLUDED

#include <stdlib.h>
#include <stdio.h>
#include <winsock2.h>

#include "inttypes.h"				// nur bei C99 konformen Compilern
#include "nodave.h"
#include "openSocket.h"
#include "userDataHandling.h"

#define DATA_HEADER_LENGTH 4

#define MAX_PDU_SIZE		960
#define MAX_ITEMS 			80		// Maximale Anzahl Items in einem Telegramm (Abh�ngig von PDU size)
									// bei PDU size 960: 12 Bytes pro Anfrage= 960/12 = 80

#define DATA_OK 	0xff			// Item-Anfrage OK
#define DATA_ERROR 	0x0a			// Item-Anfrage Fehler (z.B. nicht vorhanden)
	
// Typen von 1 Byte L�nge
#define TRANSPORT_SIZE_BIT  	1
#define TRANSPORT_SIZE_BYTE 	2
#define TRANSPORT_SIZE_CHAR  	3
// Typen von 2 Byte L�nge
#define TRANSPORT_SIZE_WORD 	4
#define TRANSPORT_SIZE_INT  	5
// Typen von 4 Byte L�nge
#define TRANSPORT_SIZE_DWORD	6
#define TRANSPORT_SIZE_DINT		7
#define TRANSPORT_SIZE_REAL  	8

// ISO 8073 PDU-Typen 
#define ISO_PDUTYPE_CR 0xE0 		// CR Connect Request
#define ISO_PDUTYPE_CC 0xD0 		// CC Connect Confirm
#define ISO_PDUTYPE_DT 0xF0 		// DT Data

#define ISOTCPminPacketLength 16

#define BUFSIZE 1024				// 512
#define TDPU_MAX_SIZETYPE 10		// 9 ld BUFSIZE
/*
 * Hilfsstruktur im die Daten des S7-Protokolls aus dem Buffer zu fischen
 */
#pragma pack(1)
typedef struct {
    uint8_t 	const1;				// immer 0x12
    uint8_t 	const2;				// immer 0x0a
    uint8_t 	const3;				// immer 0x10
    uint8_t 	transportSize;
    uint16_t 	length;
    uint16_t 	dbNumber;
    uint8_t 	areaCode;
    uint8_t 	bitAddress0;
    uint8_t 	bitAddress1;
    uint8_t 	bitAddress2;
} itemAddress_t;
#pragma pack()

typedef struct {
	unsigned char	area;			// Datenbereich
	unsigned int 	db;				// DB/M Nummer
	unsigned char 	transportSize;
	unsigned int  	bytepos;		// Startadresse Byte
	unsigned char 	bitpos;			// Startadresse Bit (nur bei Bittypen)
	unsigned int  	length;			// L�nge der Daten	(in Byte)
	unsigned char 	retCode;		// Return-Code (0xff = OK)
	unsigned char 	buf[BUFSIZE];	// Daten oder Result
} item_t;

#define ISO8073_STARTPOS 11 		// Startposition der variablen Daten im ISO Telegramm
#define ISO8073_MAX_DATA_LEN 32
typedef struct {
	uc type;
	uc len;
	uc data[ISO8073_MAX_DATA_LEN];
} ISO8073_param_t;

// Globale Variablen
char ConnectedAddress[25];			// "000.000.000.000:0000000" IP-Adresse und Port des Clients
SOCKET acceptSocket;
SOCKET connectedSocket;
daveInterface * di;
daveConnection * dc;
_daveOSserialType fds;
unsigned short PDUsequenceNumber; 	// Sequence Number aus PDU Header
int ItemCount;						// Aktuelle Anzahl an Items

// Prototypen
int mydaveReadISOPacket(daveInterface * di,uc *b, uc *disconnected) ;
int __declspec (dllexport) getItemCount(void);

int __declspec (dllexport) startNoDaveServer(void);
void __declspec (dllexport) cleanUpNoDaveServer(void);
int __declspec (dllexport) waitForRequests(item_t item[], int * reqType);

int fillItems(PDU * p, item_t item[], uc func);
void __declspec (dllexport) printItems(item_t item[]);
void __declspec (dllexport) sendReadResponse(item_t item[]);
void _sendReadResponse(daveConnection * dc, item_t item[]);
void __declspec (dllexport) sendWriteResponse(item_t item[]);
void _sendWriteResponse(daveConnection * dc, item_t item[]);
void _sendSZLResponse(daveConnection * dc);

void _handleSystemMessage(daveConnection * dc, PDU * p1);

void  setPDUheaderSequenceNumber(PDU * p, unsigned short seq);
unsigned short getPDUheaderSequenceNumber(PDU * p);
void fillItemAddress(itemAddress_t *it, PDU * p, int pos);
void answerOpenS7connectRequest(daveConnection * dc, PDU * crPDU);
void sendConnectConfirm(daveConnection * dc);
int startTCPServer(SOCKET *acceptSocket, SOCKET *connectedSocket);
void __declspec (dllexport) getClientAddress(char * strAddress);
int startWinsock(void);
int ISO8073getToken(ISO8073_param_t *par, uc **ptr_data, uc *ptr_last);
#endif
