/* CheckItemTest.c
 *
 * Testprogramm f�r den noDaveServer
 * Sendet auf alle Lese- und Schreibanfragen Erfolg zur�ck. Die Inhalte k�nnen zuf�llig sein,
 * da der Speicher vorher nicht initialisiert wurde.
 */

#include <stdlib.h>
#include <stdio.h>
#include <windows.h>
#include <signal.h>

#include "noDaveServer.h"

BOOL CtrlHandler( DWORD fdwCtrlType )
{
	switch( fdwCtrlType ) {
		case CTRL_C_EVENT:
			printf( "\nExited with Ctrl+C\n" );
			exit(0);
			return TRUE;
		default:
			return FALSE;
	}
}

int main(void)
{
    int res, reqType, i;
	int connected = 0;
	char str[25];
	item_t *Item = (item_t *) malloc(MAX_ITEMS * sizeof(item_t));
	printf("Programmende mit Ctrl+C\n\n");
	
	SetConsoleCtrlHandler( (PHANDLER_ROUTINE) CtrlHandler, TRUE );
	
	printf("Starte Server...\n");
	
	while (1) {
		res = startNoDaveServer();
		do {
			res = startNoDaveServer();
			printf ("timeout...\n");
			
		} while (res < 0);
		
		if (res > 0) {
			printf("Fehler bei startNoDaveServer()\n");
			free(Item);
			cleanUpNoDaveServer();
			return -1;
		}
		getClientAddress(str);
		printf("Verbunden mit: %s\n", str);		
		
		connected = 1;	
		do {
			res = waitForRequests(Item, &reqType);
			if (res > 0) {
				printf("Anfrage ueber %d Items gekommen\n", res);
				if (reqType == daveFuncRead) {
					printf("daveFuncRead bekommen\n");					
					for (i = 0; i < ItemCount; i++) {
						Item[i].retCode = DATA_OK; 	// OK	
					}
					sendReadResponse(Item);
				} 
				else if (reqType == daveFuncWrite) {
					printf("daveFuncWrite bekommen\n");				
					for (i = 0; i < ItemCount; i++) {
						Item[i].retCode = DATA_OK; // OK
					}
					sendWriteResponse(Item);
				}
			}
			else if (res == -1) { // Disconnect
				connected = 0;
			}
			Sleep(1000);
		} while (connected == 1);
		printf("Setze Verbindung zurueck\n");
		cleanUpNoDaveServer();
	}	// while(1)
	
	free(Item);
	cleanUpNoDaveServer();	
	return EXIT_SUCCESS;
}

