/* 
 * noDaveServer 
 * TCP/IP-Server f�r eine S7-Verbindung
 *
 * Copyright (C) 2008 Thomas Wiens, th.wiens@gmx.de
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 * 
 */

#include "noDaveServer.h"

////////////////////////////////////////////////////////////////////////////
/// Initialisierungen und starten des TCP-Sockets
/// @return 0 bei Erfolg
////////////////////////////////////////////////////////////////////////////
int __declspec (dllexport) startNoDaveServer(void)
{	
	int res;
    // Server starten und auf connect warten
	res = startTCPServer(&acceptSocket, &connectedSocket);
    if (res != 0)
    	return res;
	
	// Initialisierungen damit die nodave-Funktionen genutzt werden k�nnen
	fds.rfd = (HANDLE) connectedSocket;
	fds.wfd = fds.rfd;   
	
	// Angenommenen Socket auf Non-Blocking setzen
	unsigned long iNonBlocking = TRUE;
	ioctlsocket(connectedSocket, FIONBIO, &iNonBlocking);
	
    di = daveNewInterface(fds, "IF1", 0, daveProtoISOTCP, daveSpeed187k);
    daveSetTimeout(di,5000000);
	
    dc = daveNewConnection(di, 2, 0, 0);    
    //daveSetDebug(daveDebugAll);
    daveSetDebug(daveDebugPrintErrors);
	ItemCount = 0; 	
	return 0;
}
////////////////////////////////////////////////////////////////////////////
/// Erweitertung von _daveReadISOPacket.
/// In "disconnected" wird eingetragen wenn der Client sich vom
/// Server getrennt hat, oder versucht die Verbindung zur�ckzusetzen (RST, ACK)
/// @return Anzahl der gelesenen Bytes (bei Erfolg)
////////////////////////////////////////////////////////////////////////////
int __declspec (dllexport) mydaveReadISOPacket(daveInterface * di,uc *b, uc *disconnected) 
{
	int res,i,length;
	int WSA_LastError;
	*disconnected = 0;
	i = recv((SOCKET)(di->fd.rfd), b, 4, 0);
	WSA_LastError = WSAGetLastError();
	// printf("recv=%d --- WSAGetLastError()=%d\n", i, WSA_LastError);
	if ((i == 0) || (WSA_LastError == WSAECONNRESET)) {
		*disconnected = 1;
		printf("mydaveReadISOPacket() recv returned %d -> Client disconnected!\n", i);
		return 0;
	}
	
	res = i;
	if (res <= 0) {
	    return 0;
	} 

   	if (res < 4) {
		return 0; /* short packet */
	}
	length = b[3] + 0x100 * b[2]; // Laenge aus TPKT
	i = recv((SOCKET)(di->fd.rfd), b + 4, length - 4, 0);
	WSA_LastError = WSAGetLastError();
	if ((i == 0) || (WSA_LastError == WSAECONNRESET)) {
		*disconnected = 1;
		printf("mydaveReadISOPacket(): recv() returned %d -> Client disconnected!\n", i);
		strcpy(ConnectedAddress, "Disconnected");
		return 0;
	}
	res += i;
	return res;
}
////////////////////////////////////////////////////////////////////////////
/// @return Anzahl der Items
////////////////////////////////////////////////////////////////////////////
int __declspec (dllexport) getItemCount(void)
{
	return ItemCount;
}
////////////////////////////////////////////////////////////////////////////
/// Trennt die Verbindungen und r�umt auf.
////////////////////////////////////////////////////////////////////////////
void __declspec (dllexport) cleanUpNoDaveServer(void)
{
	closesocket(acceptSocket);
	closesocket(connectedSocket);
	WSACleanup();
	acceptSocket = NULL;
	connectedSocket = NULL;
}
////////////////////////////////////////////////////////////////////////////
/// Liest verwertbare Pakete aus der Schnittstelle ein.
/// Verbindungsanfragen werden automatisch beantwortet
/// Wird -1 zur�ckgegeben wurde die Verbindung getrennt
/// @return Anzahl der Items wenn eine Lese- oder Schreibanfrage gekommen ist
////////////////////////////////////////////////////////////////////////////
int __declspec (dllexport) waitForRequests(item_t item[], int * reqType)
{
	PDU pdu;	
	int res, retValue;
	uc disconnected;
	retValue = 0;
	res = mydaveReadISOPacket(dc->iface, dc->msgIn, &disconnected);
	if (res > ISOTCPminPacketLength) {
		switch (dc->msgIn[5]) {
			case ISO_PDUTYPE_CR:
				printf ("'CR Connect Request' received...\n");
				sendConnectConfirm(dc); // Connect Response
				*reqType = 0;
				break;
			case ISO_PDUTYPE_DT:
				printf ("'DT Data Packet' received...\n");
				// Nun an die Auswertung des S7-Protokolls...
				_daveSetupReceivedPDU(dc, &pdu);
				// Sequence Number aus Paket auslesen (wird bei Antwort wieder zur�ckgesendet)
				PDUsequenceNumber = getPDUheaderSequenceNumber(&pdu);
				if (pdu.plen > 2) {	
					if (pdu.param[0] == daveFuncOpenS7Connection) {
						// printf(" 'OpenS7Connection' Request received...\n");
						*reqType = daveFuncOpenS7Connection;
						answerOpenS7connectRequest(dc, &pdu);		
						retValue = 0;
					}
					else if (pdu.param[0] == daveFuncRead) {
						// printf(" 'daveFuncRead' Request received...\n");	
						fillItems(&pdu, item, daveFuncRead);
						*reqType = daveFuncRead;
						retValue = ItemCount;
					}
					else if (pdu.param[0] == daveFuncWrite) {
						// printf(" 'daveFuncWrite' Request received...\n");	
						fillItems(&pdu, item, daveFuncWrite);
						*reqType = daveFuncWrite;
						retValue = ItemCount;
					}
					else if (pdu.param[0] == 0x00 &&
							pdu.param[1] == 0x01 &&
							pdu.param[2] == 0x12 ) { /* &&
							pdu.param[5] == 0x55 */
						// printf("'Read SZL' received\n");		
						//_sendSZLResponse(dc);
						_handleSystemMessage(dc, &pdu);
						*reqType = -1;
						retValue = 0;
					} 
					else {
						printf(" Unable to deal with Parameter 0x%02X ...\n", pdu.param[0]);
					}
				}
				break;
			default:
				printf ("Unknown PDU-Type!\n");
				*reqType = -1;
				break;
		}
	}
	else {
		if (disconnected == 1) {
			retValue = -1;
		}
	}
	return retValue;	
}
////////////////////////////////////////////////////////////////////////////
// Test Handling einer Systemnachricht
void _handleSystemMessage(daveConnection * dc, PDU * p1)
{
	PDU pr;
	int rc;
	int haveResp = 0;
	
	pr.header = dc->msgOut + dc->PDUstartO;	
	
	uc pa[]=	{
		0x00,
		0x01,
		0x12,
		0x08,
		0x12,
		0x84,
		0x01,
		0x01, //0x07, sequence number?
		0x00,
		0x00,
		0x00,
		0x00 
	};
	
	uc data[] = {
		0x0a,	// Data error
		0x00,	// transport size 0
		0x00,	// length (word) 0
		0x00
	};

	haveResp = handleSystemMessage(p1, &pr);

	if (haveResp != 0) {
		printf("Have something to response...\n");
	} else {
		printf("Have no idea what to response...\n");	
		return;
		_daveInitPDUheader(&pr, 7);
		_daveAddParam(&pr, pa, sizeof(pa));		
		_daveAddData(&pr, data, sizeof(data));
	}	
	setPDUheaderSequenceNumber(&pr, PDUsequenceNumber);
	rc = _daveSendISOPacket(dc, pr.hlen + pr.plen + pr.dlen + 3); // +3 wegen L�nge des ISO-Headers

}

////////////////////////////////////////////////////////////////////////////
/// Parameter PDU auswerten und in Items eintragen.
/// @return 0 wenn ohne Fehler
////////////////////////////////////////////////////////////////////////////
int fillItems(PDU * p, item_t item[], uc func)
{   
	itemAddress_t param; 
	int i;
	int numItems;
	int itemPos = 2;
	
	numItems = (int) p->param[1]; // Number of Items to read
	
	if (p->plen < (2 + numItems * sizeof(itemAddress_t))) {
		printf("Received parameter-length is lower than the declared value of items in PDU! plen = %d\n", p->plen);
		return 1;
	}
	if (numItems > MAX_ITEMS) {
		printf("Requested amount of items is greater than max. Requested amount: %d, Max: %d\n", numItems, MAX_ITEMS);
		numItems = MAX_ITEMS;
	}
	
	ItemCount = 0;
	
	for (i = 0; i < numItems; i++) {		
		fillItemAddress(&param, p, itemPos);

    	int bytepos, bitpos, x;
    	x = param.bitAddress2 + param.bitAddress1 * 0x100 + param.bitAddress0 * 0x10000;
		bytepos = x / 8;
		bitpos = x % 8;
		
		item[i].area 			= param.areaCode;
		item[i].area 			= param.areaCode;
		item[i].transportSize 	= param.transportSize;
		item[i].db 				= param.dbNumber;
		item[i].bytepos 		= bytepos;
		item[i].bitpos 			= bitpos;
		item[i].length 			= param.length;	
		// Alle Laengen immer auf Bytes
		if ((item[i].transportSize == TRANSPORT_SIZE_WORD) || (item[i].transportSize == TRANSPORT_SIZE_INT)) {
			item[i].length *= 2;
		}
		else if (item[i].transportSize >= TRANSPORT_SIZE_DWORD && item[i].transportSize <= TRANSPORT_SIZE_REAL) { // DWORD, DINT oder REAL
			item[i].length *= 4;
		}
		else if (item[i].transportSize == 0x1c|| item[i].transportSize == 0x1d) { // Timers or Counters
			item[i].length *= 2;
		}
    	itemPos += sizeof(itemAddress_t);
		ItemCount++;
	}
	
	// Datenbereiche kopieren
	if (func == daveFuncWrite) {
		uc * dataPtr;
		int bytelen;
		dataPtr = p->data;
		for (i = 0; i < ItemCount; i++) {
			// L�nge bestimmen
			bytelen = *(dataPtr + 3) | (*(dataPtr + 2) << 8);
			if (*(dataPtr + 1) == 4) { // Transport Size 4=Bits
				bytelen /= 8;
			}				
			dataPtr += DATA_HEADER_LENGTH;  // Zeiger auf eigentliche Daten setzen
			memcpy(item[i].buf, dataPtr, bytelen);
			
			if(i < ItemCount - 1) {
				bytelen = bytelen + (bytelen % 2);  	// the PLC places extra bytes at the end of all 
			}											// but last result, if length is not a multiple 
														// of 2
														
			dataPtr += bytelen; // Zeiger auf n�chsten Datensatz
		}
	}	
	return 0;
}
////////////////////////////////////////////////////////////////////////////
/// Ausgeben der Itemdaten.
////////////////////////////////////////////////////////////////////////////
void __declspec (dllexport) printItems(item_t item[])
{
	int i, x, length;
	printf("\n*************  Items overall: %d   ***************\n", ItemCount);
	for (i = 0; i < ItemCount; i++) {
		printf("\n------------------------------------------------------\n");
		printf("Item No. %d \n", i + 1);
		printf("\t Area          : %d - %s \n", item[i].area, daveAreaName(item[i].area));
		printf("\t Transport Size: %d - ", item[i].transportSize);
		if (item[i].transportSize == TRANSPORT_SIZE_BYTE)
			printf("BYTE\n");
		else if ((item[i].transportSize == TRANSPORT_SIZE_WORD) || (item[i].transportSize == TRANSPORT_SIZE_INT))
			printf("WORD\n");
		else if (item[i].transportSize == TRANSPORT_SIZE_BIT)
			printf("BIT\n");
		else
			printf("unknown!\n");
		printf("\t DB/M/E/A No.  : %d \n", item[i].db);
		printf("\t Byteposition  : %d \n", item[i].bytepos);
		printf("\t Bitposition   : %d \n", item[i].bitpos);
		printf("\t Length        : %d \n", item[i].length);
		printf("\t Data          : ");
		length = item[i].length;
		if (length > BUFSIZE)
			length = BUFSIZE;
		for (x = 0; x < length; x++) {
			printf("0x%02X ", item[i].buf[x]);
		}		
	}
	printf("\n------------------------------------------------------\n");
}
////////////////////////////////////////////////////////////////////////////
// Wrapper um _sendReadResponse, damit daveConnection nicht nach aussen gef�hrt werden muss.
// Sendet die Daten in den Items als ReadResponse zur�ck
////////////////////////////////////////////////////////////////////////////
void __declspec (dllexport) sendReadResponse(item_t item[])
{
	_sendReadResponse(dc, item);
}

void _sendReadResponse(daveConnection * dc, item_t item[])
{
	int rc;
	int i;
	PDU p;	
	uc pa[]=	{
		daveFuncRead, 	// In diesem Fall ein Read Response
		1, 				// Anzahl der Items (1..20)
	};
	
	// Data Header (4 byte each),
	uc dataHeader[] = {
		0xff, // return code, 0xff = OK
		0x04, // transport size 4= BIT, 9= BYTE
		0x00, // Laenge ins Bits/bytes
		0x00  // Laenge ins Bits/bytes
	};
	
	// Anzahl der Items und L�nge setzen
	pa[1] = (uc) ItemCount;

	p.header = dc->msgOut + dc->PDUstartO;	  
    _daveInitPDUheader(&p, 3); // Type 0x03 -> Response
    _daveAddParam(&p, pa, sizeof(pa));		

	for (i = 0; i < ItemCount; i++) {	
		dataHeader[0] = (uc) item[i].retCode;
		dataHeader[1] = 0x04; // Transportsize: 0x04=BIT, 0x09=BYTE
		dataHeader[2] = (item[i].length * 8) >> 8;
		dataHeader[3] = item[i].length * 8;
		_daveAddData(&p, dataHeader, 4);
		// Laenge des Datenbereichs ist (ausser beim letzten Item) immer ein Vielfaches von 2, darum 
		// bekommt das Paket ggf. ein F�llbyte	
		if (((i + 1) < ItemCount) && (item[i].length % 2)) {
			item[i].length++;
		}
		_daveAddData(&p, item[i].buf, item[i].length);
	}	
	setPDUheaderSequenceNumber(&p, PDUsequenceNumber);
	rc = _daveSendISOPacket(dc, p.hlen + p.plen + p.dlen + 3); // +3 wegen L�nge des ISO-Headers
}
////////////////////////////////////////////////////////////////////////////
/// Wrapper um _sendWriteResponse, damit daveConnecttion nicht nach aussen gef�hrt werden muss.
/// Schickt die Item Daten als WriteResponse zur�ck
////////////////////////////////////////////////////////////////////////////
void __declspec (dllexport) sendWriteResponse(item_t item[])
{
	_sendWriteResponse(dc, item);
}
void _sendWriteResponse(daveConnection * dc, item_t item[])
{
	int rc, i;
	PDU p;	
	uc pa[]=	{
		daveFuncWrite, 			// Write Response
		1, 			   			// Anzahl der Items (1..20)
	};
	uc data[MAX_ITEMS];    		// Maximal 20 Items
	
	pa[1] = (uc) ItemCount;
	
	p.header = dc->msgOut + dc->PDUstartO;	
	_daveInitPDUheader(&p, 3); // Type 0x03 -> Response
    _daveAddParam(&p, pa, sizeof(pa));		
	
	for (i = 0; i < ItemCount; i++) {
		data[i] = item[i].retCode;
	}
	
	_daveAddData(&p, data, i);
	setPDUheaderSequenceNumber(&p, PDUsequenceNumber);
	rc = _daveSendISOPacket(dc, p.hlen + p.plen + p.dlen + 3); // +3 wegen L�nge des ISO-Headers
}
////////////////////////////////////////////////////////////////////////////
/// Tr�gt im PDU-Header die Sequenz-Nummer ein.
////////////////////////////////////////////////////////////////////////////
void  setPDUheaderSequenceNumber(PDU * p, unsigned short seq) {
	((PDUHeader*)p->header)->number = seq;
}
////////////////////////////////////////////////////////////////////////////
/// Liest aus dem PDU-Header die Sequenz-Nummer.
/// @return Sequenz-Nummer
////////////////////////////////////////////////////////////////////////////
unsigned short getPDUheaderSequenceNumber(PDU * p) {
	unsigned short seq;
	seq = ((PDUHeader*)p->header)->number;
	return seq;
}
////////////////////////////////////////////////////////////////////////////
void fillItemAddress(itemAddress_t *it, PDU * p, int pos)
{
	it->const1 			= p->param[pos];
    it->const2 			= p->param[pos + 1];
    it->const3 			= p->param[pos + 2];
    it->transportSize 	= p->param[pos + 3];
	it->length 			= ((p->param[pos + 4] << 8 ) | (p->param[pos + 5]));
	it->dbNumber		= ((p->param[pos + 6] << 8 ) | (p->param[pos + 7]));
    it->areaCode 		= p->param[pos + 8];
    it->bitAddress0 	= p->param[pos + 9];
    it->bitAddress1 	= p->param[pos + 10];
    it->bitAddress2 	= p->param[pos + 11];	
}
////////////////////////////////////////////////////////////////////////////
/// Beantwortet einen Connect Request (S7-Verbindung).
////////////////////////////////////////////////////////////////////////////
void answerOpenS7connectRequest(daveConnection * dc, PDU * crPDU)
{
	int rc;
	// Bedeutung der Bytes noch nicht n�her rausgesucht
	uc confS7Con[]={ 
		0x02, // ISO8073: Length
		0xf0, // ISO8073: Type (DT Data)
		0x80, // ISO8073: Last Data Unit
		0x32, // Immer 0x32
		0x03, // Type 0x03 (12 Bytes lang)
		0x00, // unknown
		0x00, // unknown
		0xff, // seq. Number
		0xff, // seq. Number 
		0x00, // length of parameters
		0x08, // length of parameters
		0x00, // length of data
		0x00, // length of data
		0x00, // ?
		0x00, // ?		
		0xf0, // Parameter: 0xf0 = Function code
		0x00, // Parameter: 
		0x00, // Parameter: 
		0x01, // Parameter: Max Number of parallel jobs
		0x00, // Parameter: 
		0x01, // Parameter: Max Number of parallel jobs 
		0x03, // Parameter:  PDU Size: 0x01e0 = 480, 0x03c0 = 960
		0xc0  // Parameter: 
	};

	confS7Con[7]  = (uc) PDUsequenceNumber;
	confS7Con[8]  = (uc) (PDUsequenceNumber >> 8);
	
	// Wenn angefragte PDU size kleiner als unsere, dann diese antworten
	int req_pdu_size;
	req_pdu_size = (crPDU->param[6] << 8) | crPDU->param[7];
	printf ("Requested PDU size: %d \n", req_pdu_size);
	if (req_pdu_size < MAX_PDU_SIZE) {
		confS7Con[21] =crPDU->param[6];
		confS7Con[22] =crPDU->param[7];
	}	
	
	memcpy(dc->msgOut + 4, confS7Con, sizeof(confS7Con));
	rc = _daveSendISOPacket(dc, sizeof(confS7Con));
}

////////////////////////////////////////////////////////////////////////////
/// Zerlegt den variablen Teil des ISO8073 Protokolls
////////////////////////////////////////////////////////////////////////////

int ISO8073getToken(ISO8073_param_t *par, uc **ptr_data, uc *ptr_last)
{
	if (*ptr_data + 3 > ptr_last) return 0; // Min 3 Bytes

	par->type = **ptr_data;
	(*ptr_data)++;
		
	par->len  = **ptr_data;
	(*ptr_data)++;

	if (((*ptr_data + par->len) > ptr_last) || (par->len > ISO8073_MAX_DATA_LEN)) return 0;
		
	for (int i = 0; i < par->len; i++) {
		par->data[i] = **ptr_data;
		(*ptr_data)++;
	}
	return par->len;
}
////////////////////////////////////////////////////////////////////////////
/// Schickt ein Connect Confirm (ISO 8073).
////////////////////////////////////////////////////////////////////////////	
void sendConnectConfirm(daveConnection * dc)
{
	int rack, slot;
	uc pduSizeType = TDPU_MAX_SIZETYPE;
	int rc;
	
	// Auf Anfrage mit CC Connect Confirm antworten
	uc cc[]={
						// *** fixed part ***
		0x11, 			// Length 17
		ISO_PDUTYPE_CC,	// 0xD0 = CC Connect Confirm
		0x00, 0x01, 	// 2, 3: Dest.Reference
		0x00, 0x01, 	// 4, 5: Source Reference
		0x00, 			// 6 :Class Option
						// *** variable part ***
		0xC0, 			// 7: Param. Code: tdpu-size
		0x01, 			// 8: Param. length 1
		0x09, 			// 9: TPDU size
		0xC1, 			// 10: Param. Code:scr-tsap
		0x02, 			// 11: Param. length 2
		0x01, 			// 12:
		0x00, 			// 13:
		0xC2, 			// 14: Param. Code: dst-tsap
		0x02, 			// 15: Param. length 2
		0x03, 			// 16:
		0x02  			// 17:
	};
	ISO8073_param_t par;
	uc *ptr;
	uc *ptr_last;
	int len;	
		
	// Fixed Part: set Destination Reference
	cc[2] = dc->msgIn[8];
	cc[3] = dc->msgIn[9];

	int dest_ref, src_ref;
	dest_ref = (dc->msgIn[6] << 8 | dc->msgIn[7]);
	src_ref  = (dc->msgIn[8] << 8 | dc->msgIn[9]);
	
	printf ("Dest. Reference: %d\nSource Reference: %d\n", dest_ref, src_ref); 
	
	// work variable part
	len = (dc->msgIn[2] << 8 | dc->msgIn[3]);	
	ptr = &dc->msgIn[ISO8073_STARTPOS]; // pointer to first parameter
	
	// check if given length is ok
	if (len > sizeof(dc->msgIn)) {
		ptr_last = &dc->msgIn[sizeof(dc->msgIn)];
	} else {
		ptr_last = &dc->msgIn[len];
	}	
	
	while (ISO8073getToken(&par, &ptr, ptr_last) > 0) {
		// Nur bekannte Parameter auswerten
		switch (par.type) {
			case 0xC0: // TDPU Size, 1 Byte
				pduSizeType = par.data[0];
				break;
			case 0xC1: // scr-tsap, 2 Byte
				cc[12] = par.data[0];
				cc[13] = par.data[1];
				break;
			case 0xC2: // dst-tsap, 2 Byte
				cc[16] = par.data[0];
				cc[17] = par.data[1];	
				break;
		}
	}	
	
	printf (" Source - TSAP       : %d . %d\n", cc[12], cc[13]);
	printf (" Connection Ressource: %d\n", dc->msgIn[17]);
	rack = cc[17] >> 5;	// Rack Bits 5-7
	slot = cc[17] & 0x1F;	// Slot Bits 0-4
	printf (" Destination - TSAP  : Rack %d / Slot %d\n", rack, slot);
		
	
	cc[9] = pduSizeType;
	if (pduSizeType > TDPU_MAX_SIZETYPE) // Abh�ngig von bufsize, Bufsize >= 2^sizetype
		pduSizeType = TDPU_MAX_SIZETYPE;	
	printf ("\n TPDU-size-type: Requested: %d, Answered %d\n", cc[9], pduSizeType);
	cc[9] = pduSizeType;
		
	
	memcpy(dc->msgOut + 4, cc, sizeof(cc));
	rc = _daveSendISOPacket(dc, sizeof(cc));
}	
////////////////////////////////////////////////////////////////////////////
/// Startet einen TCP-Socket der auf eingehende Verbindungen auf Port 102 wartet.
/// Ist der Socket schon erzeugt worden, so wird mittels select() auf einen Verbindung gewartet
/// @return 0 bei Erfolg (Socket gestartet, Client verbunden) <0 bei Timeout, >0 bei Fehler
////////////////////////////////////////////////////////////////////////////
int startTCPServer(SOCKET *acceptSocket, SOCKET *connectedSocket)
{
	long rc;
	SOCKADDR_IN addr;
	int WSA_LastError;  
	int remoteAddrLen;
	FD_SET fdSet; 
	// Timeout parameter
	struct timeval tv = { 0 };
	tv.tv_sec = 1;
	strcpy(ConnectedAddress, "Disconnected");
	
	if (*acceptSocket == NULL) { // Socket noch nicht gestartet
		// Winsock starten
		rc = startWinsock();
		if(rc != 0) {
			printf("Error: startWinsock failed with error code: %ld\n",rc);
			return 1;
		} 
	
		// Socket erstellen
		*acceptSocket = socket(AF_INET, SOCK_STREAM, 0);
		if(*acceptSocket == INVALID_SOCKET) {
			WSA_LastError = WSAGetLastError();
			printf("Error: Was not able to create Socket. Error code: %d\n", WSA_LastError);
			return 2;
			// return WSA_LastError;
		} 
	
		// Socket binden
		memset(&addr, 0, sizeof(SOCKADDR_IN));
		addr.sin_family = AF_INET;												// AF_INET Standard TCP Protokoll
		addr.sin_port = htons(102);												// Port
		addr.sin_addr.s_addr = ADDR_ANY;										// IP-Adresse, hier wird von beliebiger angenommen
		rc = bind(*acceptSocket, (SOCKADDR*)&addr, sizeof(SOCKADDR_IN));		// Socket mit lokaler Adresse verbinden
		if(rc == SOCKET_ERROR) {
			WSA_LastError = WSAGetLastError();
			printf("Fehler: bind failed with error code: %d\n", WSAGetLastError());
			return 3;
			// return WSA_LastError;
		} 

		rc = listen(*acceptSocket, 1);											// Auf max. 1 Verbindung warten
		if(rc == SOCKET_ERROR)	{
			printf("Error: listen failed with error code: %d\n", WSAGetLastError());
			return 4;
		}
	}
	
	FD_ZERO(&fdSet);
	FD_SET(*acceptSocket, &fdSet); 

	rc = select(0, &fdSet, NULL, NULL, &tv);
	if (rc > 0) {
		if(rc == SOCKET_ERROR) {
			printf("Error: select failed with: %d\n",WSAGetLastError());
			return 5;
		}
		if(FD_ISSET(*acceptSocket, &fdSet)) { 
			remoteAddrLen = sizeof(SOCKADDR_IN);
			*connectedSocket = accept(*acceptSocket, (SOCKADDR*)&addr, &remoteAddrLen);    
			sprintf(ConnectedAddress, "%s:%d", inet_ntoa(addr.sin_addr), ntohs(addr.sin_port));
			return 0;
		}
	
	}
	return -1; // Timeout
}
////////////////////////////////////////////////////////////////////////////
void getClientAddress(char * strAddress)
{
	strcpy(strAddress, ConnectedAddress);
}

////////////////////////////////////////////////////////////////////////////
int startWinsock(void)
{
	WSADATA wsa;
	return WSAStartup(MAKEWORD(2, 0), &wsa);
}
