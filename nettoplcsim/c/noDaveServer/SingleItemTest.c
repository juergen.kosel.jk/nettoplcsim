/* SingleItemTest.c
 * 
 * Testprogramm f�r den noDaveServer
 * 
 * Stellt eine SPS mit einem Item mit dem Speicherbereich DB2.DBW10 zur Verf�gung (Read/write).
 * Der angefragte Bereich muss aber 2 Bytes lang sein.
 */

#include <stdlib.h>
#include <stdio.h>

#include "noDaveServer.h"

int main(void)
{
    int res, reqType, i;
	int connected = 0;
	char str[25];
	item_t *Item = (item_t *) malloc(MAX_ITEMS * sizeof(item_t));
	
	int DB2DBW10 = 1234;
	
	printf("Starte Server...\n");
	
	while (1) {
		res = startNoDaveServer();
		do {
			res = startNoDaveServer();
			printf ("timeout...\n");
			
		} while (res < 0);
		
		if (res > 0) {
			printf("Fehler bei startNoDaveServer()\n");
			free(Item);
			cleanUpNoDaveServer();
			return -1;
		}
		getClientAddress(str);
		printf("Verbunden mit: %s\n", str);		
		
		connected = 1;	
		do {
			res = waitForRequests(Item, &reqType);
			if (res > 0) {
				printf("Anfrage ueber %d Items gekommen\n", res);
				if (reqType == daveFuncRead) {
					printf("daveFuncRead bekommen\n");					
					for (i = 0; i < ItemCount; i++) {
						// Testantwort f�r DB2.DBW10
						if((Item[i].db == 2) && (Item[i].bytepos == 10) && (Item[i].length = 2)) {
							printf("Setze Item[%d] auf Wert von DB2.DBW10, DB2.DBW10 = %d \n", i + 1, DB2DBW10);			
							Item[i].buf[0] = DB2DBW10 >> 8;
							Item[i].buf[1] = DB2DBW10;
							Item[i].retCode = DATA_OK; 	// OK		
						}
						else {
							Item[i].retCode = DATA_ERROR;   	// Item nicht verf�gbar
							Item[i].length = 0;			// L�nge der Daten = 0
						}
					}
					sendReadResponse(Item);
				} 
				else if (reqType == daveFuncWrite) {
					printf("daveFuncWrite bekommen\n");				
					for (i = 0; i < ItemCount; i++) {
						// Testantwort f�r DB2.DBW10
						if((Item[i].db == 2) && (Item[i].bytepos == 10) && (Item[i].length = 2)) {	
							printf("Setze DB2DBW10 von %d ", DB2DBW10);
							DB2DBW10 = Item[i].buf[1] | (Item[i].buf[0] << 8);
							printf(" auf %d\n", DB2DBW10);
							Item[i].retCode = DATA_OK; // OK
						}
						else {
							Item[i].retCode = DATA_ERROR;   	// Item nicht verf�gbar
						}
					}
					sendWriteResponse(Item);
				}
			}
			else if (res == -1) { // Disconnect
				connected = 0;
			}
			Sleep(1);
		} while (connected == 1);
		printf("Setze Verbindung zurueck\n");
		cleanUpNoDaveServer();
	}	// while(1)
	
	free(Item);
	cleanUpNoDaveServer();	
	return EXIT_SUCCESS;
}

