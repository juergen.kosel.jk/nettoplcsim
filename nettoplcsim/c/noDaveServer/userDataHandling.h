/* userDataHandling.h
 *
 * Author: Thomas Wiens, Juli 2009
 *
 */
 
#ifndef USERDATAHANDLING_HEADER_INCLUDED
#define USERDATAHANDLING_HEADER_INCLUDED

#include "nodave.h"
#include <stdlib.h>
#include <stdio.h>

extern int daveDebug;

#define runModeStop 0
#define runModeRun 1


int handleSystemMessage(PDU * p1,PDU * p2);
int handleSZL(int number, int index, PDU *p2);

/*
typedef struct _szl {
    uc ID;
    uc index;
    uc elements;
    uc elementLength;
}
*/

#endif
