@echo off
rem ****************************************************************
rem ****************************************************************
rem *** Erzeugt die Dokumentation f�r Nettoplcsim aus
rem *** den asciidoc Dateien.
rem ***
rem *** Thomas Wiens, 04.02.2018
rem ***
rem *** Notwendige Programme:
rem *** - Microsoft HTML Workshop
rem *** - cygwin
rem ***   - cygwin Module: asciidoc, dblatex, texlive-collection-langgerman 
rem ***   - cygwin Module: ruby, rubygem
rem ***
rem ***   Auf der cygwin console: 
rem ***     $ gem install asciidoctor
rem ***     $ gem install asciidoctor-pdf --pre
rem ****************************************************************
rem ****************************************************************

rem *** Einstellungen f�r cygwin
set BASH="C:\cygwin\bin\bash.exe"

rem *** Ist CHERE_INVOKING gesetzt, bleibt cygwin nach Aufruf im 
rem *** aktuellen Arbeitsverzeichnis
set CHERE_INVOKING=1

rem *** Einstellungen HTML-Workshop zur Generierung von chm Dateien
set HTMLWORKSHOP="C:\Program Files\HTML Help Workshop\hhc.exe"

rem *** Aktuelles Verzeichnis
set DOCFOLDER=%cd%

rem *** Eingabedateien (ohne Endung .adoc)
set DOCFILE_DE=NetToPLCsim-Manual-de
set DOCFILE_EN=NetToPLCsim-Manual-en

echo.
echo ****************************************************************
echo Entferne bisherige Ziel-Ordner...
echo ****************************************************************

rmdir /s /q html
rmdir /s /q pdf
rmdir /s /q chm
rmdir /s /q www

echo.
echo ****************************************************************
echo Erzeuge HTML-Dokumentation...
echo ****************************************************************
mkdir html
%BASH% --login -c "~/bin/asciidoctor -o html/%DOCFILE_DE%.html %DOCFILE_DE%.adoc" -t
%BASH% --login -c "~/bin/asciidoctor -o html/%DOCFILE_EN%.html %DOCFILE_EN%.adoc" -t

echo.
echo ****************************************************************
echo Kopiere das images Verzeichnis in den html Ordner...
echo ****************************************************************

xcopy images\* html\images /s /i

echo.
echo ****************************************************************
echo Erzeuge PDF...
echo ****************************************************************

mkdir pdf
%BASH% --login -c "~/bin/asciidoctor-pdf -o pdf/%DOCFILE_DE%.pdf %DOCFILE_DE%.adoc"
%BASH% --login -c "~/bin/asciidoctor-pdf -o pdf/%DOCFILE_EN%.pdf %DOCFILE_EN%.adoc"

echo.
echo ****************************************************************
echo Erzeuge DocBook...
echo ****************************************************************

%BASH% --login -c "asciidoc -a lang=de -v -b docbook -d article -o %DOCFILE_DE%.xml %DOCFILE_DE%.adoc"
%BASH% --login -c "asciidoc -a lang=en -v -b docbook -d article -o %DOCFILE_EN%.xml %DOCFILE_EN%.adoc"

echo.
echo ****************************************************************
echo Erzeuge htmlhelp (CHM)...
echo ****************************************************************

mkdir chmtemp
xcopy images\* chmtemp\images /s /i
copy docbook-xsl-htmlhelp.css chmtemp
mv %DOCFILE_DE%.xml chmtemp\%DOCFILE_DE%.xml
mv %DOCFILE_EN%.xml chmtemp\%DOCFILE_EN%.xml
%BASH% --login -c "a2x -v -f htmlhelp --icons --stylesheet=docbook-xsl-htmlhelp.css -D chmtemp chmtemp/%DOCFILE_DE%.xml"
%BASH% --login -c "a2x -v -f htmlhelp --icons --stylesheet=docbook-xsl-htmlhelp.css -D chmtemp chmtemp/%DOCFILE_EN%.xml"
%HTMLWORKSHOP% %DOCFOLDER%\chmtemp\%DOCFILE_DE%.hhp
%HTMLWORKSHOP% %DOCFOLDER%\chmtemp\%DOCFILE_EN%.hhp

mkdir chm
copy chmtemp\%DOCFILE_DE%.chm chm\%DOCFILE_DE%.chm
copy chmtemp\%DOCFILE_EN%.chm chm\%DOCFILE_EN%.chm

echo.
echo ****************************************************************
echo Erzeuge html fuer www
echo ****************************************************************

mkdir www
xcopy images\* www\images /s /i
%BASH% --login -c "~/bin/asciidoctor -d book -o www/index.html www-index.adoc"
%BASH% --login -c "~/bin/asciidoctor -d book -o www/license.html www-license.adoc"
%BASH% --login -c "~/bin/asciidoctor -d book -o www/history.html www-history.adoc"

echo.
echo ****************************************************************
echo Entferne Arbeitsverzeichnis...
echo ****************************************************************

rmdir /s /q chmtemp

echo.
echo Fertig!
echo.

pause
